function load(sectionName){
    $('#contents').fadeOut( 
        200, 
        function() {
			dettach();
            $('#contents').load(sectionName + ".html" , function(){ $('#contents').fadeIn(200 , function(){ attach('#contents') })});
        }
    )
}

function startUp() {
    load('inici');
    $(".menu").bind("click", function(){ load($(this).attr("id")) });
}
